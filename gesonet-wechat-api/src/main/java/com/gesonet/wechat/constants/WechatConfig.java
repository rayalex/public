package com.gesonet.wechat.constants;

import com.gesonet.common.utils.PropertiesUtils;

public interface WechatConfig {

	
	/**服务号的应用号:微信公众号身份的唯一标识。审核通过后，在微信収送的邮件中查看。*/
	String CFG_APPID = PropertiesUtils.getString("wx.appId");
	/**服务号的应用密码:JSAPI接口中获取openid，审核后在公众平台开启开収模式后可查看。*/
	String CFG_APP_SECRECT = PropertiesUtils.getString("wx.Appsecret"); 
	/**商户号:商户ID，身份标识，在微信収送的邮件中查看。*/
	String CFG_MCH_ID = PropertiesUtils.getString("wx.Mchid");
	/**API密钥:商户支付密钥 Key。登录微信商户后台，进入栏目【账户设置】【密码安全】【API安全】【API密钥】*/
	String CFG_API_KEY = PropertiesUtils.getString("wx.api_key");
	/**服务号的配置token*/
	String CFG_TOKEN = PropertiesUtils.getString("wx.token");
	/**签名加密方式*/
	String CFG_SIGN_TYPE = PropertiesUtils.getString("wx.sign_type");
	/**微信支付证书存放路径地址*/
	String CFG_CERT_PATH = PropertiesUtils.getString("wx.cert_path");
	
	String CFG_TRADE_TYPE_JSAPI = PropertiesUtils.getString("wx.trade_type.jsapi");
	String CFG_TRADE_TYPE_NATIVE = PropertiesUtils.getString("wx.pay.trade_type.native");
	String CFG_TRADE_TYPE_APP = PropertiesUtils.getString("wx.pay.trade_type.app");
	
	
	/**微信支付获得code后的回调url*/
	String URL_OAUTH_BACK=PropertiesUtils.getString("wx.pay.oauth_back_url");
	/**微信支付统一接口的回调action,微信服务器用来通知支付结果*/
	String URL_NOTIFY = PropertiesUtils.getString("wx.pay.notify_url");
	/**微信支付成功支付后跳转的地址(内部使用)*/
	String URL_SUCCESS = PropertiesUtils.getString("wx.pay.success_url");
	
	
	
	/**微信基础接口地址*/
	
	/**获取token接口(GET)*/
	String URL_TOKEN = PropertiesUtils.getString("wx.interface.token_url");
	/**通过token获取ticket接口(GET)*/
	String URL_TICKET = PropertiesUtils.getString("wx.interface.ticket");
	/**获取微信服务器IP地址接口(GET)*/
	String URL_CALLBACK_IP = PropertiesUtils.getString("wx.interface.callback_ip");
	/**oauth2授权接口(GET)*/
	String URL_OAUTH2 = PropertiesUtils.getString("wx.interface.oauth2_url");
	/**刷新access_token接口（GET）*/
	String URL_REFRESH_TOKEN = PropertiesUtils.getString("wx.interface.refresh_token_url");
	/**网页授权获取用户信息(GET)*/
	//scope 参数视各自需求而定，这里用scope=snsapi_base 不弹出授权页面直接授权目的只获取统一支付接口的openid
	String URL_OAUTH2_AUTHORIZE = PropertiesUtils.getString("wx.interface.oauth2_authorize_url");
	
	
	
	/**
	 * 微信支付接口地址
	 */
	/**微信支付统一接口(POST)*/
	String URL_UNIFIED_ORDER = PropertiesUtils.getString("wx.interface.pay.unified_order_url");
	/**微信退款接口(POST)*/
	String URL_REFUND = PropertiesUtils.getString("wx.interface.pay.refund_url");
	/**订单查询接口(POST)*/
	String URL_CHECK_ORDER = PropertiesUtils.getString("wx.interface.pay.check_order_url");
	/**关闭订单接口(POST)*/
	String URL_CLOSE_ORDER = PropertiesUtils.getString("wx.interface.pay.close_order_url");
	/**退款查询接口(POST)*/
	String URL_CHECK_REFUND = PropertiesUtils.getString("wx.interface.pay.check_refund_url");
	/**对账单接口(POST)*/
	String URL_DOWNLOAD_BILL = PropertiesUtils.getString("wx.interface.pay.download_bill_url");
	/**短链接转换接口(POST)*/
	String URL_SHORT = PropertiesUtils.getString("wx.interface.pay.short_url");
	/**接口调用上报接口(POST)*/
	String URL_REPORT = PropertiesUtils.getString("wx.interface.pay.report_url");
}
