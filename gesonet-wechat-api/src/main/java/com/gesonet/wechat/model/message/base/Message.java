package com.gesonet.wechat.model.message.base;

/**
 * @ClassName: Message 
 * @Description: 消息基础数据
 * @author QinXiaoXiang
 * @date 2016年7月29日 下午2:07:38 
 */
public class Message {
	
	public static final String TYPE_TEXT = "text";
	public static final String TYPE_IMAGE = "image";
	public static final String TYPE_LINK = "link";
	public static final String TYPE_VIDEO = "video";
	public static final String TYPE_SHORT_VIDEO = "shortvideo";
	public static final String TYPE_VOICE = "voice";
	public static final String TYPE_LOCATION = "location";
	
	/**开发者微信号*/
	private String ToUserName;
	/**发送方帐号（一个OpenID）*/
	private String FromUserName;
	/**消息创建时间 （整型）*/
	private long CreateTime;
	/**消息类型*/
	private String MsgType;
	/**消息id，64位整型*/
	private long MsgId;

	public String getToUserName() {
		return ToUserName;
	}

	public void setToUserName(String toUserName) {
		ToUserName = toUserName;
	}

	public String getFromUserName() {
		return FromUserName;
	}

	public void setFromUserName(String fromUserName) {
		FromUserName = fromUserName;
	}

	public long getCreateTime() {
		return CreateTime;
	}

	public void setCreateTime(long createTime) {
		CreateTime = createTime;
	}

	public String getMsgType() {
		return MsgType;
	}

	public void setMsgType(String msgType) {
		MsgType = msgType;
	}

	public long getMsgId() {
		return MsgId;
	}

	public void setMsgId(long msgId) {
		MsgId = msgId;
	}
	
	
}
