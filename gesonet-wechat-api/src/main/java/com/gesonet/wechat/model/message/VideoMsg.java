package com.gesonet.wechat.model.message;

import com.gesonet.wechat.model.message.base.Message;

/**
 * @ClassName: VideoMsg 
 * @Description: 视频消息
 * @author QinXiaoXiang
 * @date 2016年7月29日 下午2:15:10 
 */
public class VideoMsg extends Message{

	/**语音消息媒体id，可以调用多媒体文件下载接口拉取数据。*/
	private String MediaId;
	/**视频消息缩略图的媒体id，可以调用多媒体文件下载接口拉取数据。*/
	private String ThumbMediaId;
	
	public String getMediaId() {
		return MediaId;
	}
	public void setMediaId(String mediaId) {
		MediaId = mediaId;
	}
	public String getThumbMediaId() {
		return ThumbMediaId;
	}
	public void setThumbMediaId(String thumbMediaId) {
		ThumbMediaId = thumbMediaId;
	}
	
	
}
