package com.gesonet.wechat.model.common;

/**
 * 微信访问Token
 * @author qin
 */
public class WxToken {

	/**获取到的凭证*/
	private String access_token;
	/**凭证有效时间，单位：秒*/
	private Integer expires_in;

	public WxToken() {
		super();
	}

	public WxToken(String access_token, Integer expires_in) {
		super();
		this.access_token = access_token;
		this.expires_in = expires_in;
	}

	public String getAccess_token() {
		return access_token;
	}

	public void setAccess_token(String access_token) {
		this.access_token = access_token;
	}

	public Integer getExpires_in() {
		return expires_in;
	}

	public void setExpires_in(Integer expires_in) {
		this.expires_in = expires_in;
	}
	
	
}
