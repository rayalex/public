package com.gesonet.wechat.model.message;

import com.gesonet.wechat.model.message.base.Message;

/**
 * @ClassName: LinkMsg 
 * @Description: 链接消息
 * @author QinXiaoXiang
 * @date 2016年7月29日 下午3:26:04 
 *
 */
public class LinkMsg extends Message{

	/**消息标题*/
	private String Title;
	/**消息描述*/
	private String Description;
	/**消息链接*/
	private String Url;
	public String getTitle() {
		return Title;
	}
	public void setTitle(String title) {
		Title = title;
	}
	public String getDescription() {
		return Description;
	}
	public void setDescription(String description) {
		Description = description;
	}
	public String getUrl() {
		return Url;
	}
	public void setUrl(String url) {
		Url = url;
	}
	
	
}
