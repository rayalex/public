package com.gesonet.wechat.model.message;

import com.gesonet.wechat.model.message.base.Message;

/**
 * @ClassName: LocationMsg 
 * @Description: 地理位置消息
 * @author QinXiaoXiang
 * @date 2016年7月29日 下午2:18:25 
 */
public class LocationMsg extends Message{

	/**地理位置维度*/
	private String Location_X;
	/**地理位置维度*/
	private String Location_Y;
	/**地图缩放大小*/
	private int Scale;
	/**地理位置信息*/
	private String Label;
	
	public String getLocation_X() {
		return Location_X;
	}
	public void setLocation_X(String location_X) {
		Location_X = location_X;
	}
	public String getLocation_Y() {
		return Location_Y;
	}
	public void setLocation_Y(String location_Y) {
		Location_Y = location_Y;
	}
	public int getScale() {
		return Scale;
	}
	public void setScale(int scale) {
		Scale = scale;
	}
	public String getLabel() {
		return Label;
	}
	public void setLabel(String label) {
		Label = label;
	}
	
}
