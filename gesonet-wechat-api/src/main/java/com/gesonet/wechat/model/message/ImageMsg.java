package com.gesonet.wechat.model.message;

import com.gesonet.wechat.model.message.base.Message;

/**
 * @ClassName: ImageMsg 
 * @Description: 图片消息
 * @author QinXiaoXiang
 * @date 2016年7月29日 下午2:13:02 
 */
public class ImageMsg  extends Message{

	/**图片链接（由系统生成）*/
	private String PicUrl;
	/**图片消息媒体id，可以调用多媒体文件下载接口拉取数据。*/
	private String MediaId;
	
	public String getPicUrl() {
		return PicUrl;
	}
	public void setPicUrl(String picUrl) {
		PicUrl = picUrl;
	}
	public String getMediaId() {
		return MediaId;
	}
	public void setMediaId(String mediaId) {
		MediaId = mediaId;
	}

	
}
