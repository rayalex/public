package com.gesonet.wechat.model.message;

import com.gesonet.wechat.model.message.base.Message;

/**
 * @ClassName: TextMsg 
 * @Description: 文本消息
 * @author QinXiaoXiang
 * @date 2016年7月29日 下午2:07:12 
 */
public class TextMsg extends Message{

	/**文本消息内容*/
	private String Content;

	public String getContent() {
		return Content;
	}

	public void setContent(String content) {
		Content = content;
	}
	
	
	
}
