package com.gesonet.wechat.api;

import java.io.IOException;
import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;

import org.jdom2.JDOMException;

public class WechatPayApi {

//	/**
//	 * 使用统一支付接口，获取prepay_id
//	 * @param openId
//	 * @return
//	 * @throws JDOMException
//	 * @throws IOException
//	 */
//	private SortedMap<Object, Object> getPrepayId(String openId) throws JDOMException, IOException {
//		//网页授权后获取传递的参数
//		String out_trade_no = request.getParameter("orderNo");
//		
//		String total_fee = request.getParameter("totalPrice");
//		ActivityOrder query = new ActivityOrder(null, null, null, ActivityOrder.STATUS_BOOK, out_trade_no);
//		ActivityOrder order = activityOrderService.selectByCondition(query);
//		if(order != null){
//			total_fee =  Integer.parseInt(new java.text.DecimalFormat("0").format(order.getTotalPrice())) + "";
//		}
//		//获取openId后调用统一支付接口https://api.mch.weixin.qq.com/pay/unifiedorder
//		String nonce_str = PayCommonUtil.createNonceStr(); 
//		String body = "product"; 
//		try {
//			body = "达帮网618抢购";//order.getProduct().getName();
//		} catch (Exception e) {
//			logger.error("get product name error", e);
//		}
//		String spbill_create_ip = request.getRemoteAddr();
//		
//		SortedMap<Object, Object> paramsForPrepayId = new TreeMap<Object, Object>();
//		paramsForPrepayId.put("appid", WechatConfigUtil.APPID);  
//		paramsForPrepayId.put("mch_id", WechatConfigUtil.MCH_ID);  
//		paramsForPrepayId.put("nonce_str", nonce_str);  //随机字符串，不长于 32位
//		paramsForPrepayId.put("body", body);  //商品描述
//		paramsForPrepayId.put("out_trade_no", out_trade_no);  //商户系统内部的订单号,32个字符内、可包含字母
//		paramsForPrepayId.put("total_fee", String.valueOf(total_fee));  //实际金额,需要进行数据计算，单位为分，不能带小数点
//		paramsForPrepayId.put("spbill_create_ip", spbill_create_ip);  //订单生成的机器 IP 16位之内
//		paramsForPrepayId.put("notify_url", WechatConfigUtil.NOTIFY_URL);  //接收微信支付成功通知
//		paramsForPrepayId.put("trade_type", WechatConfigUtil.TRADE_TYPE_JSAPI); //交易类型:JSAPI、NATIVE、APP 
//		paramsForPrepayId.put("openid", openId);//用户在商户 appid下的唯一标识，trade_type为 JSAPI时，此参数必传
//		
//		String sign = PayCommonUtil.createSign("UTF-8", paramsForPrepayId);
//		
//		paramsForPrepayId.put("sign", sign);
//		
//		String requestXML = PayCommonUtil.getRequestXml(paramsForPrepayId);
//		
//		logger.debug("调用微信统一支付接口参数:{}", requestXML);
//		//最后我们要将这些参数以POST方式调用微信统一支付接口
//		String result = CommonUtil.httpsRequest(WechatConfigUtil.UNIFIED_ORDER_URL, "POST", requestXML);
//		logger.debug("微信统一支付接口 返回消息:" + result);
//		/**步骤2：使用统一支付接口，获取prepay_id*/
//		Map<String, String> retsultMap = XMLUtil.doXMLParse(result);//解析微信返回的信息，以Map形式存储便于取值
//		//将返回的值传入到支付jsp页面，在支付jsp页面调用支付接口
//		SortedMap<Object, Object> paramsForPage = new TreeMap<Object, Object>();
//		//map中有return_code和result_code两个字段,只有当这两个字段的值都为SUCCESS的时候，prepay_id才有值
//		String return_code = retsultMap.get("return_code");
//		String result_code = retsultMap.get("result_code");
//		
//		if ("SUCCESS".equalsIgnoreCase(return_code) && "SUCCESS".equalsIgnoreCase(result_code)) {
//			String prepay_id = retsultMap.get("prepay_id");
//			logger.debug("prepay_id={}",prepay_id);
//			String prepay_id2 = "prepay_id="+prepay_id;
//			//JSAPI支付接口只需要六个参数
//	        paramsForPage.put("appId", WechatConfigUtil.APPID);
//	        paramsForPage.put("timeStamp", PayCommonUtil.createTimestamp());
//	        paramsForPage.put("nonceStr", PayCommonUtil.createNoncestr());
//	        paramsForPage.put("package", prepay_id2);
//	        paramsForPage.put("signType", WechatConfigUtil.SIGN_TYPE);
//	        String paySign =  PayCommonUtil.createSign("UTF-8", paramsForPage);
//	        paramsForPage.put("paySign", paySign);  //paySign的生成规则和Sign的生成规则一致
//	        //JSAPI支付接口只需要六个参数
//	        //在微信浏览器里面打开H5网页中执行JS调起支付。接口输入输出数据格式为JSON。
//	        paramsForPage.put("packageValue", prepay_id2);    //这里用packageValue是预防package是关键字在js获取值出错
//	        paramsForPage.put("sendUrl", WechatConfigUtil.SUCCESS_URL+"618/success.html"); //付款成功后跳转的页面
//	        paramsForPage.put("isWechat5", String.valueOf(isWechat5()));//判断微信版本号，用于前面提到的判断用户手机微信的版本是否是5.0以上版本。
//		} else {
//			paramsForPage.put("errMsg", result_code);
//			logger.debug("return_code={}",return_code);
//			logger.debug("result_code={}",result_code);
//		}
//		return paramsForPage;
//	}
	
}
