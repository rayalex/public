package com.gesonet.wechat.api;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;

import com.gesonet.wechat.model.common.WxTicket;
import com.gesonet.wechat.model.common.WxToken;
import com.gesonet.wechat.utils.Sign;

public class JsSDKApi {
	
	
	public static final String WX_VOICE_SUFFIX = ".amr";
	public static final String H5_VOICE_SUFFIX = ".mp3";
	
	/**
	 * 因为微信要求缓存token和ticket，因此不建议使用此方法
	 * @Title: getSignatureData 
	 * @Description: 获取JS-SDK需要的加密数据
	 * @param: url
	 * @return: Map<String,String> 
	 * @throws
	 */
	public static Map<String,String> getSignatureData(String url){
		try {
			WxToken token = CommonApi.getToken();
			String tokenStr = token.getAccess_token();
			if(StringUtils.isNotBlank(tokenStr)){
				WxTicket wxTicket = CommonApi.getTicketByToken(tokenStr);
				String ticket = wxTicket.getTicket();
				return Sign.sign(ticket, url);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return new HashMap<>();
	}
	

}
