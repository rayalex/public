package com.gesonet.wechat.api;

import java.text.MessageFormat;

import org.apache.commons.lang.StringUtils;

import com.gesonet.common.utils.HttpsUtils;
import com.gesonet.common.utils.json.JsonUtils;
import com.gesonet.wechat.constants.WechatConfig;
import com.gesonet.wechat.model.common.WxTicket;
import com.gesonet.wechat.model.common.WxToken;

public class CommonApi implements WechatConfig{

	
	/**
	 * 获取access_token
	 * @return 
	 * @throws Exception
	 */
	public static WxToken getToken() throws Exception{
		String url = MessageFormat.format(URL_TOKEN, CFG_APPID, CFG_APP_SECRECT);
		String json = HttpsUtils.httpsRequest(url, HttpsUtils.METHOD_GET, StringUtils.EMPTY);
		return (WxToken) JsonUtils.TO_OBJ(json, WxToken.class);
	}
	
	/**
	 * @Title: getTicketByToken 
	 * @Description: 通过token获取ticket
	 * @param: token
	 * @return: WxTicket 
	 * @throws Exception
	 */
	public static WxTicket getTicketByToken(String token) throws Exception{
		String url = MessageFormat.format(URL_TICKET, token);
		String json = HttpsUtils.httpsRequest(url, HttpsUtils.METHOD_GET, StringUtils.EMPTY);
		return (WxTicket) JsonUtils.TO_OBJ(json, WxTicket.class);
	}
	
	
	public static String getCallbackIP(String token){
		String url = MessageFormat.format(URL_CALLBACK_IP, token);
		String json = HttpsUtils.httpsRequest(url, HttpsUtils.METHOD_GET, StringUtils.EMPTY);
		return json;
	}
	
}
