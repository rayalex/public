package com.gesonet.oauth2.api;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.gesonet.oauth2.util.TokenUtil;

/**
 * 
 * @ClassName: OauthOsc 
 * @Description: 开源中国
 * @author QinXiaoXiang
 * @date 2016年8月9日 下午2:08:19 
 *
 */
public class OauthOsc extends Oauth {
	private static final String AUTH_URL = "http://www.oschina.net/action/oauth2/authorize"; // 授权
	private static final String TOKEN_URL = "http://www.oschina.net/action/openapi/token"; // tonken
	private static final String USER_INFO_URL = "http://www.oschina.net/action/openapi/user"; // 用户信息
	private static final String TWEET_PUB = "http://www.oschina.net/action/openapi/tweet_pub"; // 动弹

	private static OauthOsc oauthOsc = new OauthOsc();
	private OauthOsc(){}
	/**
	 * 用于链式操作
	 * @return OauthOsc
	 */
	public static OauthOsc me() {
		return oauthOsc;
	}
	
	/**
	 * 获取授权url
	 * @param state OAuth2.0标准协议建议，利用state参数来防止CSRF攻击
	 * @return String 返回类型
	 */ 
	public String getAuthorizeUrl(String state) {
		return super.getAuthorizeUrl(AUTH_URL, state);
	}

	/**
	 * 获取token
	 * @param code 换取token
	 * @return String 返回类型
	 */
	public String getTokenByCode(String code) {
		Map<String, String> params = new HashMap<String, String>();
		params.put("code", code);
		params.put("client_id", getAppId());
		params.put("client_secret", getAppSecret());
		params.put("grant_type", "authorization_code");
		params.put("redirect_uri", getRedirectUri());
		String token = TokenUtil.getAccessToken(super.doGet(TOKEN_URL, params));
		LOGGER.debug(token);
		return token;
	}

	/**
	 *  获取用户信息
	 * @param accessToken AccessToken
	 * @return JSONObject
	 */
	public JSONObject getUserInfo(String accessToken) {
		Map<String, String> params = new HashMap<String, String>();
		params.put("access_token", accessToken);
		String userInfo = super.doGet(USER_INFO_URL, params);
		JSONObject dataMap = JSON.parseObject(userInfo);
		LOGGER.debug(dataMap.toJSONString());
		return dataMap;
	}
	
	/**
	 * 发送文字动弹
	 * @param accessToken AccessToken
	 * @param msg 动态内容
	 * @return JSONObject
	 */
	public JSONObject tweetPub(String accessToken, String msg) {
		Map<String, String> params = new HashMap<String, String>();
		params.put("access_token", accessToken);
		params.put("msg", msg);
		return JSON.parseObject(super.doPost(TWEET_PUB, params));
	}

	/**
	 * 根据code一步获取用户信息
	 * @param code oauth code
	 * @return JSONObject 返回类型
	 */
	public JSONObject getUserInfoByCode(String code) {
		String accessToken = getTokenByCode(code);
		if (StringUtils.isBlank(accessToken)) {
			throw new RuntimeException("accessToken is Blank!");
		}
		JSONObject dataMap = getUserInfo(accessToken);
		dataMap.put("access_token", accessToken);
		return dataMap;
	}
	
	@Override
	public Oauth getSelf() {
		return this;
	}
}
