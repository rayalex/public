package com.gesonet.oauth2.api;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;

import com.gesonet.oauth2.model.WechatToken;
import com.gesonet.oauth2.util.HttpKitExt;
import com.gesonet.oauth2.util.json.JsonUtils;

/**
 * @ClassName: OauthWechat 
 * @Description: 微信认证
 * @author QinXiaoXiang
 * @date 2016年8月9日 下午2:10:26 
 *
 */
public class OauthWechat extends Oauth {
	
	
	private static final String AUTH_URL_MOBILE = "https://open.weixin.qq.com/connect/oauth2/authorize";
	private static final String AUTH_URL_PC = "https://open.weixin.qq.com/connect/qrconnect";
	private static final String TOKEN_URL = "https://api.weixin.qq.com/sns/oauth2/access_token";
//	private static final String TOKEN_INFO_URL = "https://api.weixin.qq.com/sns/userinfo";
	private static final String USER_INFO_URL = "https://api.weixin.qq.com/sns/userinfo";

	private static OauthWechat oauthWechat = new OauthWechat();
	
	public OauthWechat(){}
	
	public OauthWechat(String redirectUri){
		super(redirectUri);
	}
	
	public OauthWechat(String appId,String appSecret,String redirectUri){
		super(appId, appSecret, redirectUri);
	}
	/**
	 * 用于链式操作
	 * @return oauthWechat
	 */
	public static OauthWechat me() {
		oauthWechat = new OauthWechat();
		return oauthWechat;
	}
	public static OauthWechat me(String redirectUri) {
		oauthWechat.setRedirectUri(redirectUri);
		return oauthWechat;
	}
	public static OauthWechat me(String appId,String appSecret,String redirectUri) {
		oauthWechat.setAppId(appId);;
		oauthWechat.setAppSecret(appSecret);
		oauthWechat.setRedirectUri(redirectUri);
		return oauthWechat;
	}

	/**
	 * 获取授权url
	 * DOC：https://open.weixin.qq.com/
	 * @param state OAuth2.0标准协议建议，利用state参数来防止CSRF攻击
	 * @return String
	 */
	public String getAuthorizeUrl(String state) {
		Map<String, String> params = new HashMap<String, String>();
		params.put("response_type", "code");
		params.put("scope", "snsapi_base");
		params.put("appid", getAppId());
		params.put("redirect_uri", getRedirectUri());
		if (StringUtils.isNotBlank(state)) {
			params.put("state", state);
		}
		return HttpKitExt.initParams(AUTH_URL_MOBILE, params);
	}

	/**
	 * 获取授权url
	 * DOC：https://open.weixin.qq.com/
	 * @param state OAuth2.0标准协议建议，利用state参数来防止CSRF攻击
	 * @return String
	 */
	public String getPCAuthorizeUrl(String state) {
		Map<String, String> params = new HashMap<String, String>();
		params.put("response_type", "code");
		params.put("scope", "snsapi_base");
		params.put("appid", getAppId());
		params.put("redirect_uri", getRedirectUri());
		if (StringUtils.isNotBlank(state)) {
			params.put("state", state);
		}
		return HttpKitExt.initParams(AUTH_URL_PC, params);
	}
	
	/**
	 * 获取token
	 * @param code 根据code换取token
	 * @return String 返回类型
	 */
	public String getTokenByCode(String code) {
		WechatToken token = getTokenInfoByCode(code);
		return token.getAccess_token();
	}
	
	/**
	 * 获取OpenId
	 * @param code 根据code换取openId
	 * @return String 返回类型
	 */
	public String getOpenIdByCode(String code) {
		WechatToken token = getTokenInfoByCode(code);
		return token.getOpenid();
	}
	
	
	/**
	 * @Title: getTokenInfoByCode 
	 * @Description: 获取完整的Token信息，带UID
	 * @param: @param code
	 * @return: String 
	 * @throws
	 */
	public WechatToken getTokenInfoByCode(String code){
		Map<String, String> params = new HashMap<String, String>();
		params.put("code", code);
		params.put("appid", getAppId());
		params.put("secret", getAppSecret());
		params.put("grant_type", "authorization_code");
		String json = super.doPost(TOKEN_URL, params);
		return (WechatToken) JsonUtils.TO_OBJ(json, WechatToken.class);
	}
	
	/**
	 * 获取用户信息
	 * @param accessToken AccessToken
	 * @param openid 用户id
	 * @return String 返回类型
	 */
	public String getUserInfo(String accessToken, String openid) {
		Map<String, String> params = new HashMap<String, String>();
		params.put("openid", openid);
		params.put("access_token", accessToken);
		String userInfo = super.doGet(USER_INFO_URL, params);
		LOGGER.debug(userInfo);
		return userInfo;
	}

	@Override
	public Oauth getSelf() {
		return this;
	}
}
