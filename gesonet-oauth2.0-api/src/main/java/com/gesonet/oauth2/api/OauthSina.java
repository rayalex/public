package com.gesonet.oauth2.api;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.gesonet.oauth2.model.SinaToken;
import com.gesonet.oauth2.util.json.JsonUtils;

/**
 * @ClassName: OauthSina 
 * @Description: 新浪
 * @author QinXiaoXiang
 * @date 2016年8月9日 下午2:10:26 
 *
 */
public class OauthSina extends Oauth {
	private static final String AUTH_URL = "https://api.weibo.com/oauth2/authorize";
	private static final String TOKEN_URL = "https://api.weibo.com/oauth2/access_token";
	private static final String TOKEN_INFO_URL = "https://api.weibo.com/oauth2/get_token_info";
	private static final String USER_INFO_URL = "https://api.weibo.com/2/users/show.json";

	private static OauthSina oauthSina = new OauthSina();
	private OauthSina(){}
	/**
	 * 用于链式操作
	 * @return OauthSina
	 */
	public static OauthSina me() {
		return oauthSina;
	}

	/**
	 * 获取授权url
	 * DOC：http://open.weibo.com/wiki/Oauth2/authorize
	 * @param state OAuth2.0标准协议建议，利用state参数来防止CSRF攻击
	 * @return String
	 */
	public String getAuthorizeUrl(String state) {
		return super.getAuthorizeUrl(AUTH_URL, state);
	}

	/**
	 * 获取token
	 * @param code 根据code换取token
	 * @return String 返回类型
	 */
	public String getTokenByCode(String code) {
		SinaToken token = getTokenInfoByCode(code);
		return token.getAccess_token();
	}
	
	/**
	 * 获取OpenId
	 * @param code 根据code换取openId
	 * @return String 返回类型
	 */
	public String getOpenIdByCode(String code) {
		SinaToken token = getTokenInfoByCode(code);
		return token.getUid();
	}
	
	
	/**
	 * @Title: getTokenInfoByCode 
	 * @Description: 获取完整的Token信息，带UID
	 * @param: @param code
	 * @return: String 
	 * @throws
	 */
	public SinaToken getTokenInfoByCode(String code){
		Map<String, String> params = new HashMap<String, String>();
		params.put("code", code);
		params.put("client_id", getAppId());
		params.put("client_secret", getAppSecret());
		params.put("grant_type", "authorization_code");
		params.put("redirect_uri", getRedirectUri());
		String json = super.doPost(TOKEN_URL, params);
		return (SinaToken) JsonUtils.TO_OBJ(json, SinaToken.class);
	}
	
	
	/**
	 * 获取TokenInfo
	 * @param accessToken AccessToken
	 * @return String 返回类型
	 */
	public String getOpenIdByToken(String accessToken) {
		Map<String, String> params = new HashMap<String, String>();
		params.put("access_token", accessToken);
		
		String jsonStr = super.doPost(TOKEN_INFO_URL, params);
		JSONObject json = JSONObject.parseObject(jsonStr);
		
		String openid = json.getString("uid");
		LOGGER.debug(openid);
		return openid;
	}
	
	/**
	 * 获取用户信息
	 * DOC：http://open.weibo.com/wiki/2/users/show
	 * @param accessToken AccessToken
	 * @param uid 用户id
	 * @return String 返回类型
	 */
	public String getUserInfo(String accessToken, String uid) {
		Map<String, String> params = new HashMap<String, String>();
		params.put("uid", uid);
		params.put("access_token", accessToken);
		String userInfo = super.doGet(USER_INFO_URL, params);
		LOGGER.debug(userInfo);
		return userInfo;
	}

	/**
	 * 根据code一步获取用户信息
	 * @param code oauth code
	 * @return JSONObject	返回类型
	 */
	public JSONObject getUserInfoByCode(String code) {
		String accessToken = getTokenByCode(code);
		if (StringUtils.isBlank(accessToken)) {
			throw new RuntimeException("Token is Blank!");
		}
		String uid = getOpenIdByToken(accessToken);
		if (StringUtils.isBlank(uid)) {
			throw new RuntimeException("accessToken is Blank!");
		}
		JSONObject dataMap = JSON.parseObject(getUserInfo(accessToken, uid));
		dataMap.put("access_token", accessToken);
		return dataMap;
	}
	
	@Override
	public Oauth getSelf() {
		return this;
	}
}
