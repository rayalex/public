package com.gesonet.oauth2.api;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;

import com.alibaba.fastjson.JSONObject;

/**
 * @ClassName: OauthRenren 
 * @Description: 人人网
 * @author QinXiaoXiang
 * @date 2016年8月9日 下午2:09:50 
 *
 */
public class OauthRenren extends Oauth {
	private static final String AUTH_URL = "https://graph.renren.com/oauth/authorize";
	private static final String TOKEN_URL = "https://graph.renren.com/oauth/token";

	private static OauthRenren oauthRenren = new OauthRenren();
	private OauthRenren(){}
	/**
	 * 用于链式操作
	 * @return OauthRenren
	 */
	public static OauthRenren me() {
		return oauthRenren;
	}
	
	/**
	 * 获取授权url
	 * DOC： http://wiki.dev.renren.com/wiki/Authentication
	 * @param state OAuth2.0标准协议建议，利用state参数来防止CSRF攻击
	 * @return String 返回类型
	 */ 
	public String getAuthorizeUrl(String state) {
		return super.getAuthorizeUrl(AUTH_URL, state);
	}

	/**
	 * 获取token
	 * @param @param code
	 * @param @return	设定文件
	 * @return String	返回类型
	 * @throws
	 */
	private String getTokenByCode(String code) {
		Map<String, String> params = new HashMap<String, String>();
		params.put("code", code);
		params.put("client_id", getAppId());
		params.put("client_secret", getAppSecret());
		params.put("grant_type", "authorization_code");
		params.put("redirect_uri", getRedirectUri());
		String token = super.doPost(TOKEN_URL, params);
		LOGGER.debug(token);
		return token;
	}

	/**
	 * 根据code一步获取用户信息
	 * @param code oauth code
	 * @return JSONObject	返回类型
	 */
	public JSONObject getUserInfoByCode(String code) {
		String tokenInfo = getTokenByCode(code);
		if (StringUtils.isBlank(tokenInfo)) {
			throw new RuntimeException("Token is Blank!");
		}
		JSONObject json = JSONObject.parseObject(tokenInfo);
		String access_token = json.getString("access_token");
		if (StringUtils.isBlank(access_token)) {
			throw new RuntimeException("Token is Blank!");
		}
		JSONObject userJson = json.getJSONObject("user");
		userJson.put("access_token", access_token);
		return userJson;
	}
	
	@Override
	public Oauth getSelf() {
		return this;
	}
}
