package com.gesonet.oauth2.util;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.gesonet.oauth2.api.OauthQQ;
import com.gesonet.oauth2.api.OauthSina;
import com.gesonet.oauth2.api.OauthWechat;
/**
 * @ClassName: OauthUtil 
 * @Description: 认证授权
 * @author QinXiaoXiang
 * @date 2016年9月5日 上午11:19:32 
 */
public class OauthUtil {
	
	private static final Logger LOG = LoggerFactory.getLogger(OauthUtil.class);
	
	public static final String TYPE_QQ = "qq";
	public static final String TYPE_SINA = "sina";
	public static final String TYPE_WECHAT = "wechat";
	
	/**
	 * @Title: getAuthorizeUrl 
	 * @Description: 通过类型来获取认证的URL
	 * @param:  type 认证类型，请见常量TYPE_* 
	 * @param:  state 随机扰码
	 * @return: String 
	 * @throws
	 */
	public static String getAuthorizeUrl(String type,String state){
		String url = StringUtils.EMPTY;
		switch (type) {
		case TYPE_SINA:
			url = OauthSina.me().getAuthorizeUrl(state);
			break;
		case TYPE_WECHAT:
			url = OauthWechat.me().getAuthorizeUrl(state);
			break;
		case TYPE_QQ:
			url = OauthQQ.me().getAuthorizeUrl(state);
			break;
		default:
			break;
		}
		LOG.debug("{}认证的url是:{}",type,url);
		return url;
	}
	
	/**
	 * @Title: getOpenIdByCode 
	 * @Description: 通过Code获取OpenId
	 * @param: type 认证类型，请见常量TYPE_* 
	 * @param: code 
	 * @return: String 
	 * @throws
	 */
	public static String getOpenIdByCode(String type,String code){
		String openId = StringUtils.EMPTY;
		switch (type) {
		case TYPE_QQ:
			openId = OauthQQ.me().getOpenIdByCode(code);
			break;
		case TYPE_SINA:
			openId = OauthSina.me().getOpenIdByCode(code);
			break;
		case TYPE_WECHAT:
			openId = OauthWechat.me().getOpenIdByCode(code);
			break;
		default:
			break;
		}
		return openId;
	}

}
