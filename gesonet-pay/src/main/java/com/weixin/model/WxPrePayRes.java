package com.weixin.model;

import com.gesonet.pay.utils.PayCommonUtil;
import com.weixin.WechatConfig;

public class WxPrePayRes {
	
	public static final String RESULT_SUCCESS = "SUCCESS";
	public static final String RESULT_FAIL = "FAIL";

	private String appId = WechatConfig.APPID;
	
	private String timeStamp = PayCommonUtil.createTimestamp();
	
	private String nonceStr = PayCommonUtil.createNonceStr();
	
	private String packageValue;
	
	private String signType = "MD5";
	
	private String paySign;
	
	private String successUrl = WechatConfig.SUCCESS_URL;
	
	private String resultCode;
	
	private String resultMsg;
	
	private String prepayId;
	
	public WxPrePayRes() {
		super();
	}

	public WxPrePayRes(String resultCode) {
		super();
		this.resultCode = resultCode;
	}
	
	public WxPrePayRes(String packageValue,String paySign, String timeStamp,String nonceStr,
			String resultCode, String resultMsg, String prepayId) {
		super();
		this.packageValue = packageValue;
		this.timeStamp = timeStamp;
		this.nonceStr = nonceStr;
		this.paySign = paySign;
		this.resultCode = resultCode;
		this.resultMsg = resultMsg;
		this.prepayId = prepayId;
	}

	public String getAppId() {
		return appId;
	}

	public void setAppId(String appId) {
		this.appId = appId;
	}

	public String getTimeStamp() {
		return timeStamp;
	}

	public void setTimeStamp(String timeStamp) {
		this.timeStamp = timeStamp;
	}

	public String getNonceStr() {
		return nonceStr;
	}

	public void setNonceStr(String nonceStr) {
		this.nonceStr = nonceStr;
	}

	public String getPackageValue() {
		return packageValue;
	}

	public void setPackageValue(String packageValue) {
		this.packageValue = packageValue;
	}

	public String getSignType() {
		return signType;
	}

	public void setSignType(String signType) {
		this.signType = signType;
	}

	public String getPaySign() {
		return paySign;
	}

	public void setPaySign(String paySign) {
		this.paySign = paySign;
	}

	public String getSuccessUrl() {
		return successUrl;
	}

	public void setSuccessUrl(String successUrl) {
		this.successUrl = successUrl;
	}

	public String getResultCode() {
		return resultCode;
	}

	public void setResultCode(String resultCode) {
		this.resultCode = resultCode;
	}

	public String getResultMsg() {
		return resultMsg;
	}

	public void setResultMsg(String resultMsg) {
		this.resultMsg = resultMsg;
	}

	public String getPrepayId() {
		return prepayId;
	}

	public void setPrepayId(String prepayId) {
		this.prepayId = prepayId;
	}
	
	
	
	
}
