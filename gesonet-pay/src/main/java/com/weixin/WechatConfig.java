package com.weixin;

import com.gesonet.pay.utils.PropertiesUtils;

public interface WechatConfig {

	
	/**
	 * 服务号相关信息
	 */
	String APPID = PropertiesUtils.getString("wx.appId");// 服务号的应用号:微信公众号身份的唯一标识。审核通过后，在微信収送的邮件中查看。
	String APP_SECRECT = PropertiesUtils.getString("wx.Appsecret");// 服务号的应用密码:JSAPI接口中获取openid，审核后在公众平台开启开収模式后可查看。
	String MCH_ID = PropertiesUtils.getString("wx.Mchid");// 商户号:商户ID，身份标识，在微信収送的邮件中查看。
	String API_KEY = PropertiesUtils.getString("wx.api_key");// API密钥:商户支付密钥 Key。登录微信商户后台，进入栏目【账户设置】【密码安全】【API安全】【API密钥】
	String TOKEN = PropertiesUtils.getString("wx.token");// 服务号的配置token
	String SIGN_TYPE = PropertiesUtils.getString("wx.sign_type");// 签名加密方式
	String CERT_PATH = PropertiesUtils.getString("wx.cert_path");// 微信支付证书存放路径地址
	//微信支付获得code后的回调url
	String OAUTH_BACK_URL=PropertiesUtils.getString("wx.pay.oauth_back_url");
	// 微信支付统一接口的回调action,微信服务器用来通知支付结果
	String NOTIFY_URL = PropertiesUtils.getString("wx.pay.notify_url");
	// 微信支付成功支付后跳转的地址(内部使用)
	String SUCCESS_URL = PropertiesUtils.getString("wx.pay.success_url");
	
	String TRADE_TYPE_JSAPI = PropertiesUtils.getString("wx.pay.trade_type.jsapi");
	String TRADE_TYPE_NATIVE = PropertiesUtils.getString("wx.pay.trade_type.native");
	String TRADE_TYPE_APP = PropertiesUtils.getString("wx.pay.trade_type.app");
	/**
	 * 微信基础接口地址
	 */
	/**获取token接口(GET)*/
	String TOKEN_URL = PropertiesUtils.getString("wx.interface.token_url");
	/**oauth2授权接口(GET)*/
	String OAUTH2_URL = PropertiesUtils.getString("wx.interface.oauth2_url");
	/**刷新access_token接口（GET）*/
	String REFRESH_TOKEN_URL = PropertiesUtils.getString("wx.interface.refresh_token_url");
	/**网页授权获取用户信息(GET)*/
	//scope 参数视各自需求而定，这里用scope=snsapi_base 不弹出授权页面直接授权目的只获取统一支付接口的openid
	String OAUTH2_AUTHORIZE_URL = PropertiesUtils.getString("wx.interface.oauth2_authorize_url");
	/**
	 * 微信支付接口地址
	 */
	/**微信支付统一接口(POST)*/
	String UNIFIED_ORDER_URL = PropertiesUtils.getString("wx.interface.pay.unified_order_url");
	/**微信退款接口(POST)*/
	String REFUND_URL = PropertiesUtils.getString("wx.interface.pay.refund_url");
	/**订单查询接口(POST)*/
	String CHECK_ORDER_URL = PropertiesUtils.getString("wx.interface.pay.check_order_url");
	/**关闭订单接口(POST)*/
	String CLOSE_ORDER_URL = PropertiesUtils.getString("wx.interface.pay.close_order_url");
	/**退款查询接口(POST)*/
	String CHECK_REFUND_URL = PropertiesUtils.getString("wx.interface.pay.check_refund_url");
	/**对账单接口(POST)*/
	String DOWNLOAD_BILL_URL = PropertiesUtils.getString("wx.interface.pay.download_bill_url");
	/**短链接转换接口(POST)*/
	String SHORT_URL = PropertiesUtils.getString("wx.interface.pay.short_url");
	/**接口调用上报接口(POST)*/
	String REPORT_URL = PropertiesUtils.getString("wx.interface.pay.report_url");
}
