package com.gesonet.pay.utils;

import org.apache.commons.configuration.Configuration;
import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.PropertiesConfiguration;

public class PropertiesUtils {

	//缺省设置
	private static String DEFAULT_PROPERTIES_NAME = "config";
	private static String DEFAULT_PROPERTIES = DEFAULT_PROPERTIES_NAME + ".properties";
	private static Configuration config;
	
	static {
		try {
			config = new PropertiesConfiguration("properties/" + DEFAULT_PROPERTIES);
		} catch (ConfigurationException e) {
			e.printStackTrace();
		}
	}
	
	
	public static String getString(String name){
		if (config != null) {
			return config.getString(name);
		}
		return "";
	}
	
	public static String getString(String name,String defaultVal){
		if (config != null) {
			return config.getString(name);
		}
		return defaultVal;
	}
	
	public static String[] getStringArray(String name,String split){
		if (config != null) {
			return config.getString(name).split(split);
		}
		return new String[0];
	}

	public static int getIntValue(String name,int defaultVal) {
		if (config != null) {
			String value = config.getString(name);
			try {
				return Integer.valueOf(value);
			} catch (Exception e) {
				return defaultVal;
			}
		}
		return defaultVal;
	}
}
