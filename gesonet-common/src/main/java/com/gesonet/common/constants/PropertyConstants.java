package com.gesonet.common.constants;

import java.io.File;

import com.gesonet.common.utils.PropertiesUtils;

public interface PropertyConstants {
	
	
	public static final String API_KEYS = PropertiesUtils.getString("api.key.array", "qinxiaoxiang");
	/**发送验证码的邮件标题*/
	public static final String SEND_VALIDATE_CODE_SUBJECT = PropertiesUtils.getString("send.validate.code.subject");
	/**发送验证码的内容*/
	public static final String SEND_VALIDATE_CODE_CONTENT = PropertiesUtils.getString("send.validate.code.content");
	
	public static final String FILE_ROOT_PATH = PropertiesUtils.getString("file.root.path");
	
	public static final String PIC_ROOT_PATH = FILE_ROOT_PATH + File.separator + "img" + File.separator;
	
	public static final String PIC_DOMAIN = PropertiesUtils.getString("pic.domain") + File.separator;
	
	public static final String RUN_ENV = PropertiesUtils.getString("run.env");
	
	public static final String SMS_URL = PropertiesUtils.getString("sms.url");
	public static final String SMS_BATCH_URL = PropertiesUtils.getString("sms.batch.url");
	public static final String SMS_ACCOUNT = PropertiesUtils.getString("sms.account");
	public static final String SMS_PASSWD = PropertiesUtils.getString("sms.passwd");
	public static final String SMS_VCODE_MSG = PropertiesUtils.getString("sms.vcode.msg");
	
	
}
