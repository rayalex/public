package com.gesonet.common.constants;

public interface ExpressConstants {

	/**验证码的缓存key {account}*/
	public static final String KEY_VALIDATE_CODE = "/validateCode/{0}";
	/**手机用户查询订单的验证码的缓存key {mdn}*/
	public static final String KEY_QUERY_VALIDATE_CODE = "/queryOrderValidateCode/{0}";
	/**手机用户确认下单的验证码的缓存key {mdn}*/
	public static final String KEY_CONFIRM_ORDER_VALIDATE_CODE = "/confirmOrderValidateCode/{0}";
	/**验证码的缓存token {account}*/
	public static final String KEY_VALIDATE_TOKEN = "/validateToken/{0}";
	
	public static final String MENU_CONFIG = "MENU_CONFIG";
	
	public static final String PIC_DOMAIN = "PIC_DOMAIN";

	public static final String RIGHTS_ID = "RIGHTS_ID";
	
	public static final String RUN_ENV = "RUN_ENV";
	
	public static final int BATCH_SIZE = 100;
	
	/**翻页参数名  每页数量*/
	public static final String PAGE_SIZE = "size";
	/**翻页参数名  查询偏移量*/
	public static final String PAGE_OFF_SET = "ofs";
	
	
}
