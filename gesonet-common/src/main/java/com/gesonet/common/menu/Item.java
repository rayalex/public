package com.gesonet.common.menu;

public class Item {
	
	private String id;
	
	private String text;
	
	private String href;
	
	private boolean closeable;
	
	public Item() {
		super();
	}

	public Item(String id, String text, String href) {
		super();
		this.id = id;
		this.text = text;
		this.href = href;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public String getHref() {
		return href;
	}

	public void setHref(String href) {
		this.href = href;
	}

	public boolean isCloseable() {
		return closeable;
	}

	public void setCloseable(boolean closeable) {
		this.closeable = closeable;
	}
	
}
