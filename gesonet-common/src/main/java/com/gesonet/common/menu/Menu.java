package com.gesonet.common.menu;

import java.util.List;

public class Menu {

	private String text;
	
	private List<Item> items;
	
	public Menu() {
		super();
	}

	public Menu(String text, List<Item> items) {
		super();
		this.text = text;
		this.items = items;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public List<Item> getItems() {
		return items;
	}

	public void setItems(List<Item> items) {
		this.items = items;
	}
	
	
}
