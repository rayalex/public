package com.gesonet.common.menu;

import java.util.List;

public class Module {
	
	private String id;
	
	private String homePage;

	private List<Menu> menu;
	
	public Module(String id, String homePage) {
		super();
		this.id = id;
		this.homePage = homePage;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getHomePage() {
		return homePage;
	}

	public void setHomePage(String homePage) {
		this.homePage = homePage;
	}

	public List<Menu> getMenu() {
		return menu;
	}

	public void setMenu(List<Menu> menu) {
		this.menu = menu;
	}
	
	
	public Module() {
		super();
	}

}
