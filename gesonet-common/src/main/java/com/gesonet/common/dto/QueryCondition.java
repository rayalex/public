package com.gesonet.common.dto;

public class QueryCondition {
	
	private String account;
	
	private String beginTime;
	
	private String endTime;
	
	private Integer limit;
	
	private Integer pageNum;
	
	private Integer start;
	
	private Integer type;
	
	private String name;
	
	
	public QueryCondition(String beginTime, String endTime, 
			Integer limit, Integer pageNum,Integer entId) {
		super();
		this.beginTime = beginTime;
		this.endTime = endTime;
		this.limit = limit;
		this.pageNum = pageNum;
		this.start = (pageNum-1)*limit;
	}



	public QueryCondition(String account, String beginTime, String endTime,
			Integer limit, Integer pageNum,Integer start) {
		super();
		this.account = account;
		this.beginTime = beginTime;
		this.endTime = endTime;
		this.limit = limit;
		this.pageNum = pageNum;
		this.start = start;
	}
	
	
	
	
	public QueryCondition(String name,String idCardNo, String entName,String beginTime, String endTime,
			Integer limit, Integer pageNum, Integer start) {
		this.account = idCardNo;
		this.beginTime = beginTime;
		this.endTime = endTime;
		this.limit = limit;
		this.pageNum = pageNum;
		this.start = start;
		this.name = name;
	}


	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public QueryCondition() {
		super();
	}

	public Integer getStart() {
		return start;
	}

	public void setStart(Integer start) {
		this.start = start;
	}

	public String getAccount() {
		return account;
	}

	public void setAccount(String account) {
		this.account = account;
	}

	public String getBeginTime() {
		return beginTime;
	}

	public void setBeginTime(String beginTime) {
		this.beginTime = beginTime;
	}

	public String getEndTime() {
		return endTime;
	}

	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}

	public Integer getLimit() {
		return limit;
	}

	public void setLimit(Integer limit) {
		this.limit = limit;
	}

	public Integer getPageNum() {
		return pageNum;
	}

	public void setPageNum(Integer pageNum) {
		this.pageNum = pageNum;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	
}
