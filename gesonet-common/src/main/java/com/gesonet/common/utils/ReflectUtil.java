package com.gesonet.common.utils;


import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.beanutils.NestedNullException;
import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.Validate;


/**
 * 反射工具类
 */
public abstract class ReflectUtil {
	

	/**
	 * 获取指定类的所有字段,排除static,final字段
	 * @param clazz 类型
	 * @return List<字段>
	 */
	public static List<Field> getFields(Class<?> clazz){
		List<Field> fieldResult = new ArrayList<Field>();
		while(clazz!=Object.class){
			try {
				Field[] fields = clazz.getDeclaredFields();
				for (Field field:fields) {
					int modifiers = field.getModifiers();
					//过滤static或final字段
					if(Modifier.isStatic(modifiers)||Modifier.isFinal(modifiers)){
						continue;
					}
					fieldResult.add(field);
				}
			} catch (Exception ignore) {}
			clazz = clazz.getSuperclass();
		}
		return fieldResult;
	}
	
	/**
	 * 获取指定类的所有字段名称,排除static,final字段
	 * @param clazz 类型
	 * @return List<字段名称>
	 */
	public static List<String> getFieldNames(Class<?> clazz){
		List<Field> fields = getFields(clazz);
		List<String> fieldNames = new ArrayList<String>(fields.size());
		for(Field field:fields){
			fieldNames.add(field.getName());
		}
		return fieldNames;
	}
	
	/**
	 * 通过反射, 获得定义 Class 时声明的父类的泛型参数的类型
	 * 如: public EmployeeDao extends BaseDao<Employee, String>
	 * @param clazz
	 * @param index
	 * @return
	 */
	public static Class<?> getSuperClassGenricType(Class<?> clazz, int index){
		Type genType = clazz.getGenericSuperclass();
		
		if(!(genType instanceof ParameterizedType)){
			return Object.class;
		}
		Type [] params = ((ParameterizedType)genType).getActualTypeArguments();
		if(index >= params.length || index < 0){
			return Object.class;
		}
		if(!(params[index] instanceof Class)){
			return Object.class;
		}
		return (Class<?>) params[index];
	}
	
	/**
	 * 通过反射, 获得 Class 定义中声明的父类的泛型参数类型
	 * 如: public EmployeeDao extends BaseDao<Employee, String>
	 * @param <T>
	 * @param clazz
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public static<T> Class<T> getSuperGenericType(Class<?> clazz){
		return (Class<T>) getSuperClassGenricType(clazz, 0);
	}
	
	
	
	/**
	 * java bean转Map
	 * @param bean
	 * @param propNames 需要放到map中的属性名称
	 * @return
	 */
	public static Map<String,Object> beanToMap(Object bean, String...propNames) {
		Map<String,Object> rtn = new HashMap<String,Object>();
		if(ArrayUtils.isEmpty(propNames)){
			List<String> fieldNames = getFieldNames(bean.getClass());
			for (String fieldName: fieldNames) {
				Object value = getProperty(bean, fieldName);
				rtn.put(fieldName, value);
			}
		}else{
			for(String propName: propNames){
				Object value = getProperty(bean, propName);
				rtn.put(propName, value);
			}
		}
		return rtn;
	}
	
	
	/**
	 * 获取属性,忽略NestedNullException
	 * 
	 * @param bean
	 * @param name
	 * @return
	 */
	public static Object getProperty(Object bean, String name){
		try {
			return PropertyUtils.getProperty(bean, name);
		} catch (NestedNullException ignore) {
			return null;
		}catch(Exception e){
			throw new RuntimeException(e);
		}
	}

	/**
	 * 获取属性类型,忽略NestedNullException
	 * 
	 * @param bean
	 * @param name
	 * @return
	 */
	public static Class<?> getPropertyType(Object bean, String name) {
		try {
			return PropertyUtils.getPropertyType(bean, name);
		} catch (NestedNullException ignore) {
			return null;
		}catch(Exception e){
			throw new RuntimeException(e);
		}
	}

	
	/**
	 * 获取n个类,相同的父类类型,如果多个相同的父类,获取最接近的的,
	 * 如果传递的对象包含Object.class 直接返回null 
	 * @param clazzs 
	 * @return 相同的父类Class
	 */
	public static Class<?> getEqSuperClass(Class<?> ...clazzs){
		Validate.notEmpty(clazzs);
		List<List<Class<?>>> container = new ArrayList<List<Class<?>>>(clazzs.length);
		for(Class<?>clazz :clazzs){
			if(clazz==Object.class)return null;
			List<Class<?>> superClazz = new ArrayList<Class<?>>(5);
			for(clazz=clazz.getSuperclass();clazz!=Object.class;clazz=clazz.getSuperclass()){
				superClazz.add(clazz);
			}
			container.add(superClazz);
		}
		List<Class<?>> result = new ArrayList<Class<?>>(5);  
		Iterator<List<Class<?>>> it = container.iterator();
		int len =0;
		while(it.hasNext()){
			if(len == 0){
				result.addAll(it.next());
			}else{
				result.retainAll(it.next());
				if(CollectionUtils.isEmpty(result)){
					break;
				}
			}
			len++;
		}
		//不管相同父类有几个,返回最接近的
		if(CollectionUtils.isNotEmpty(result)){
			return result.get(0);
		}
		return Object.class;
	}
	
}
