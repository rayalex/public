package com.gesonet.common.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *  poi-3.8-beta3-20110606.jar
 *  poi-ooxml-3.8-beta3-20110606.jar
 *  poi-ooxml-schemas-3.8-beta3-20110606.jar
 *  xmlbeans-2.3.0.jar
 *  jar包官网下载地址：http://poi.apache.org/download.html
 *  下载poi-bin-3.8-beta3-20110606.zip
 */
public class ExcelUtils {
	
	private static final Logger logger = LoggerFactory.getLogger(ExcelUtils.class);

	/** 总行数 */
	private int totalRows = 0;
	/** 总列数 */
	private int totalCells = 0;
	/** 错误信息 */
	private String errorInfo;
	/** 构造方法 */
	public ExcelUtils() {}
	/**
	 * @描述：得到总行数
	 * @参数：@return
	 * @返回值：int
	 */
	public int getTotalRows() {
		return totalRows;
	}

	/**
	 * @描述：得到总列数
	 * @参数：@return
	 * @返回值：int
	 */
	public int getTotalCells() {
		return totalCells;
	}

	/**
	 * @描述：得到错误信息
	 * @参数：@return
	 * @返回值：String
	 */
	public String getErrorInfo() {
		return errorInfo;
	}

	/**
	 * @描述：验证excel文件
	 * @参数：@param filePath 文件完整路径
	 * @参数：@return
	 * @返回值：boolean
	 */
	public boolean validateExcel(String filePath) {
		/** 检查文件名是否为空或者是否是Excel格式的文件 */
		if (filePath == null || !(WDWUtil.isExcel2003(filePath) || WDWUtil.isExcel2007(filePath))) {
			errorInfo = "文件名不是excel格式";
			return false;
		}
		/** 检查文件是否存在 */
		File file = new File(filePath);
		if (file == null || !file.exists()) {
			errorInfo = "文件不存在";
			return false;
		}
		return true;
	}

	/**
	 * @描述：根据文件名读取excel文件
	 * @参数：@param filePath 文件完整路径
	 * @参数：@return
	 * @返回值：List
	 */
	public List<List<String>> read(String filePath) {
		List<List<String>> dataLst = new ArrayList<List<String>>();
		InputStream is = null;
		try {
			/** 验证文件是否合法 */
			if (!validateExcel(filePath)) {
				System.out.println(errorInfo);
				return null;
			}
			/** 判断文件的类型，是2003还是2007 */
			boolean isExcel2003 = true;
			if (WDWUtil.isExcel2007(filePath)) {
				isExcel2003 = false;
			}
			/** 调用本类提供的根据流读取的方法 */
			File file = new File(filePath);
			is = new FileInputStream(file);
			dataLst = read(is, isExcel2003);
			is.close();
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			if (is != null) {
				try {
					is.close();
				} catch (IOException e) {
					is = null;
					e.printStackTrace();
				}
			}
		}
		/** 返回最后读取的结果 */
		return dataLst;
	}

	/**
	 * @描述：根据流读取Excel文件
	 * @参数：@param inputStream
	 * @参数：@param isExcel2003
	 * @参数：@return
	 * @返回值：List
	 */
	public List<List<String>> read(InputStream inputStream, boolean isExcel2003) {
		List<List<String>> dataLst = null;
		try {
			/** 根据版本选择创建Workbook的方式 */
			Workbook wb = null;
			if (isExcel2003) {
				wb = new HSSFWorkbook(inputStream);
			} else {
				wb = new XSSFWorkbook(inputStream);
			}
			dataLst = read(wb);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return dataLst;
	}

	/**
	 * 
	 * @描述：读取数据
	 * @参数：@param Workbook
	 * @参数：@return
	 * @返回值：List<List<String>>
	 */
	private List<List<String>> read(Workbook wb) {
		List<List<String>> dataLst = new ArrayList<List<String>>();
		/** 得到第一个shell */
		Sheet sheet = wb.getSheetAt(0);
		/** 得到Excel的行数 */
		this.totalRows = sheet.getPhysicalNumberOfRows();
		/** 得到Excel的列数 */
		if (this.totalRows >= 1 && sheet.getRow(0) != null) {
			this.totalCells = sheet.getRow(0).getPhysicalNumberOfCells();
		}
		/** 循环Excel的行 */
		for (int r = 0; r < this.totalRows; r++) {
			Row row = sheet.getRow(r);
			if (row == null) {
				continue;
			}
			List<String> rowLst = new ArrayList<String>();
			/** 循环Excel的列 */
			for (int c = 0; c < this.getTotalCells(); c++) {
				Cell cell = row.getCell(c);
				String cellValue = "";
				if (null != cell) {
					cell.setCellType(HSSFCell.CELL_TYPE_STRING);
					cellValue = cell.getStringCellValue();
					// 以下是判断数据的类型
//					switch (cell.getCellType()) {
//					case HSSFCell.CELL_TYPE_NUMERIC: // 数字
//						cellValue = cell.getNumericCellValue() + "";
//						break;
//					case HSSFCell.CELL_TYPE_STRING: // 字符串
//						cellValue = cell.getStringCellValue();
//						break;
//					case HSSFCell.CELL_TYPE_BOOLEAN: // Boolean
//						cellValue = cell.getBooleanCellValue() + "";
//						break;
//					case HSSFCell.CELL_TYPE_FORMULA: // 公式
//						cellValue = cell.getCellFormula() + "";
//						break;
//					case HSSFCell.CELL_TYPE_BLANK: // 空值
//						cellValue = "";
//						break;
//					case HSSFCell.CELL_TYPE_ERROR: // 故障
//						cellValue = "非法字符";
//						break;
//					default:
//						cellValue = "未知类型";
//						break;
//					}
				}
				rowLst.add(cellValue);
			}
			if(notBlankRow(rowLst)){
				/** 保存第r行的第c列 */
				dataLst.add(rowLst);
			}else{
				//一整行都是空的话就认为后面都为空，退出解析
				break;
			}
		}
		return dataLst;
	}

	/**
	 * 非空行
	 * @param row
	 * @return
	 */
	private boolean notBlankRow(List<String> row){
		for (String column : row) {
			if(StringUtils.isNotBlank(column)){
				return true;
			}
		}
		return false;
	}
	/**
	 * 
	 * @描述：main测试方法
	 * @参数：@param args
	 * @参数：@throws Exception
	 * @返回值：void
	 */
	public static void main(String[] args) throws Exception {
		ExcelUtils poi = new ExcelUtils();
		// List<List<String>> list = poi.read("d:/aaa.xls");
		List<List<String>> list = poi.read("c:/book.xlsx");
		if (list != null) {
			for (int i = 0; i < list.size(); i++) {
				System.out.print("第" + (i) + "行");
				List<String> cellList = list.get(i);
				for (int j = 0; j < cellList.size(); j++) {
					// System.out.print(" 第" + (j + 1) + "列值：");
					System.out.print("    " + cellList.get(j));
				}
			}
		}
	}
	
	public static void exportExcel(String title, List<List<String>> list, OutputStream out) {
		long start = System.currentTimeMillis();
		logger.info("开始写入到Excel，数据量：{}" , list.size() );
		// 声明一个工作薄
		@SuppressWarnings("resource")
		XSSFWorkbook workbook = new XSSFWorkbook();
		// 生成一个表格
		XSSFSheet sheet = workbook.createSheet(title);
		// 产生表格标题行
		// 遍历集合数据，产生数据行
		for (int i = 0; i < list.size(); i++) {
			XSSFRow row = sheet.createRow(i);
			List<String> columns = list.get(i);
			for (short j = 0; j < columns.size(); j++) {
				XSSFCell cell = row.createCell(j);
				try {
					String column = columns.get(j);
					cell.setCellValue(column);
				} catch (Exception e) {
					e.printStackTrace();
				} 
			}
		}
		try {
			workbook.write(out);
			logger.info("数据写入到Excel耗时：{} 秒！" , (System.currentTimeMillis()-start)/1000 );
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
}



/**
 * @描述：工具类
 */
class WDWUtil {
	/**
	 * @描述：是否是2003的excel，返回true是2003
	 * @参数：@param filePath 文件完整路径
	 * @参数：@return
	 * @返回值：boolean
	 */
	public static boolean isExcel2003(String filePath) {
		return filePath.matches("^.+\\.(?i)(xls)$");
	}

	/**
	 * @描述：是否是2007的excel，返回true是2007
	 * @参数：@param filePath 文件完整路径
	 * @参数：@return
	 * @返回值：boolean
	 */
	public static boolean isExcel2007(String filePath) {
		return filePath.matches("^.+\\.(?i)(xlsx)$");
	}
	
}
