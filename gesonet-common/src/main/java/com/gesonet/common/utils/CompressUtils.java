package com.gesonet.common.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.zip.GZIPInputStream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CompressUtils {
	
	private static final Logger logger = LoggerFactory.getLogger(CompressUtils.class);

	/**
	 * @author QXX
	 * @date 2014年10月10日 下午5:34:18
	 * @Description: 按行读取GZIP文件 
	 * @param file gzip文件
	 * @return List<String> gzip文件每行内容的集合
	 * @throws
	 */
	public static List<String> readGzFileToLines(File file){
		Scanner sc = null;
		FileInputStream fis = null;
		InputStream is = null;
		List<String> lines = new ArrayList<String>();
		try {
			fis = new FileInputStream(file);
			is = new GZIPInputStream(fis);
			sc = new Scanner(is);
			while (sc.hasNextLine()) {
				lines.add(sc.nextLine());
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
			logger.error("read file error:" + file.getAbsolutePath());
		}finally{
			if (sc != null) {
				sc.close();
			}
			if (fis != null) {
				try {
					fis.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			if (is != null) {
				try {
					is.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			
		}
		return lines;
	}
}
