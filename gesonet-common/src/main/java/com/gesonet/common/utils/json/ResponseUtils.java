package com.gesonet.common.utils.json;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class ResponseUtils {

//	private static final Logger logger = LoggerFactory
//			.getLogger(JsonUtils.class);

	private static final SimpleDateFormat sdf = new SimpleDateFormat(
			"yyyyMMddHHmmss");

	public static final String RESPONSE_SUCCESS = "00000"; // 成功
	private static final String RESPONSE_FAILURE = "00001"; // 失败
	private static final String APIKEY_INVALID = "10000"; // API_KEY不合法
	private static final String REQ_PARAM_INVALID = "10001"; // 参数无效
	private static final String IP_LIMITED = "10002"; // IP访问限制
	private static final String RESPONSE_EXCEPTION = "10003"; // 未知异常
	private static final String TOKEN_INVALID = "10004"; // Token 无效,鉴权失败
	public static final String UPLOAD_FILE_INVALID = "10005"; // 上传文件过大
	public static final String ACCOUNT_EXIST = "20000"; // 帐号已存在
	public static final String ACCOUNT_NOT_EXIST = "20001"; // 帐号不存在
	public static final String DATA_TOO_MUCH = "20002";//数据过多
	public static final String ENTERPRISE_EXIST = "20003";//企业已经存在
	public static final String ACCOUNT_UNLOGIN = "20004";//帐号未登录
	public static final String NO_PERMISSION = "20005";//操作无权限
	public static final String NOT_ENOUGH_POINT = "20006";//操作无权限
	public static final String PURCHASE_FAIL = "20007";//简历购买失败
	public static final String ENT_NOT_EXIST = "20008";//企业信息不存在
	public static final String VCODE_INVALID = "20009";//验证码无效
	public static final String ACCOUNT_DISABLED = "20010";//账号不可用
	public static final String HAD_TICKET = "30000";//已获取门票
	public static final String OUT_OF_TICKET = "30001";//门票已抢完了

	public static String RESPONSE(String code,Object data) {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("code", code);
		map.put("data", data);
		map.put("timestamp", sdf.format(new Date()));
		return JsonUtils.toJSONString(map);

	}
	
	public static String VCODE_INVALID(Object data) {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("code", VCODE_INVALID);
		map.put("data", data);
		map.put("timestamp", sdf.format(new Date()));
		return JsonUtils.toJSONString(map);
	}
	
	public static String ENTERPRISE_EXIST(Object data) {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("code", ENTERPRISE_EXIST);
		map.put("data", data);
		map.put("timestamp", sdf.format(new Date()));
		return JsonUtils.toJSONString(map);
	}
	
	public static String ACCOUNT_EXIST(Object data) {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("code", ACCOUNT_EXIST);
		map.put("data", data);
		map.put("timestamp", sdf.format(new Date()));
		return JsonUtils.toJSONString(map);
	}
	
	public static String ACCOUNT_NOT_EXIST(Object data) {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("code", ACCOUNT_NOT_EXIST);
		map.put("data", data);
		map.put("timestamp", sdf.format(new Date()));
		return JsonUtils.toJSONString(map);
	}
	
	
	public static String STATUS_OK(Object data) {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("code", RESPONSE_SUCCESS);
		map.put("data", data);
		map.put("timestamp", sdf.format(new Date()));
		return JsonUtils.toJSONString(map);
	}

	/**
	 * 参数无效
	 * @return
	 */
	public static String REQUEST_PARAM_INVALID() {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("code", REQ_PARAM_INVALID);
		map.put("timestamp", sdf.format(new Date()));
		return JsonUtils.toJSONString(map);
	}
	
	/**
	 * API_KEY授权码无效
	 * @return
	 */
	public static String REQUEST_APIKEY_INVALID() {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("code", APIKEY_INVALID);
		map.put("timestamp", sdf.format(new Date()));
		return JsonUtils.toJSONString(map);
	}

	/**
	 * Token 无效,鉴权失败
	 * 
	 * @return
	 */
	public static String REQUEST_TOKEN_INVALID() {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("code", TOKEN_INVALID);
		map.put("timestamp", sdf.format(new Date()));
		return JsonUtils.toJSONString(map);
	}

	/**
	 * Ip不合法
	 * 
	 * @return
	 */
	public static String REQUEST_IP_LIMITED() {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("code", IP_LIMITED);
		map.put("timestamp", sdf.format(new Date()));
		return JsonUtils.toJSONString(map);
	}

	/**
	 * 出现异常
	 * 
	 * @return
	 */
	public static String EXCEPTION_ERROR() {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("code", RESPONSE_EXCEPTION);
		map.put("timestamp", sdf.format(new Date()));
		return JsonUtils.toJSONString(map);
	}

	
	public static String REQUEST_RESPONSE_FAILURE() {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("code", RESPONSE_FAILURE);
		map.put("timestamp", sdf.format(new Date()));
		return JsonUtils.toJSONString(map);
	}


}
