package com.gesonet.common.utils;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;

import org.apache.commons.lang3.StringUtils;

public class FileUtils {
	
	static String basePath = "";
	
	static {
		basePath = Thread.currentThread().getContextClassLoader().getResource("").getPath();
	}
	
	public static String getSysResourcePath(){
		return basePath;
	}

	public static String readFile(String fileName) {
		String result = StringUtils.EMPTY;
		try {
			File file = new File(fileName);
			BufferedReader reader = null;
			reader = new BufferedReader(new FileReader(file));
			String line = null;
			StringBuffer buffer = new StringBuffer();
			while ((line = reader.readLine()) != null) {
				buffer.append(line);
			}
			reader.close();
			result = buffer.toString();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}

	public static boolean writeToFile(String lineStr, String file, Boolean isAppend) {
		try {
			BufferedWriter bWriter = new BufferedWriter(new FileWriter(file, isAppend));
			bWriter.write(lineStr);
			bWriter.close();
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	public static void main(String[] args) {
		String str = readFile("d:\\1.txt");
		System.out.println(str);
	}
}
